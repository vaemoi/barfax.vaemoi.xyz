const
  fs = require(`fs`),
  path = require(`path`);

// Set folder paths for different image types
const
  faviconInputDir = path.join(__dirname, `../assets/favicon`),
  pngInputDir = path.join(__dirname, `../assets/png`),
  pngOutputDir = path.join(__dirname, `../public/assets/img/png`),
  svgInputDir = path.join(__dirname, `../assets/svg`),
  svgOutputDir = path.join(__dirname, `../public/assets/img/svg`),
  webRoot = path.join(__dirname, `../public`);

const copyDir = (src, dest) => {
  const fileNames = fs.readdirSync(src);

  if (fileNames.length) {

    if (!fs.existsSync(dest)) {
      fs.mkdirSync(dest);
    }

    for (let index = fileNames.length - 1; index >= 0; index--) {
      const
        srcFile = path.join(src, fileNames[index]),
        destFile = path.join(dest, fileNames[index]);

      if (fs.existsSync(srcFile)) {
        fs.copyFileSync(srcFile, destFile);
      }
    }
  } else {
    console.log(`${src} was empty -- skipping`)
  }
}

// Execute
copyDir(svgInputDir, svgOutputDir);
copyDir(faviconInputDir, webRoot);
copyDir(pngInputDir, pngOutputDir);
